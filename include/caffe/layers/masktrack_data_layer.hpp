#ifndef CAFFE_MASKTRACK_DATA_LAYER_HPP_
#define CAFFE_MASKTRACK_DATA_LAYER_HPP_

#include <string>
#include <utility>
#include <vector>
#include <tuple>

#include "caffe/blob.hpp"
#include "caffe/data_transformer.hpp"
#include "caffe/internal_thread.hpp"
#include "caffe/layer.hpp"
#include "caffe/layers/base_data_layer.hpp"
#include "caffe/proto/caffe.pb.h"


namespace caffe {

cv::Mat load_input(const std::string& base_dir,
                   const std::tuple<std::string, std::string, std::string>& files,
                   const int height, const int width, const bool is_color,
                   int* img_height=NULL, int* img_width=NULL);

template <typename Dtype>
class MaskTrackDataLayer : public ImageDimPrefetchingDataLayer<Dtype> {
 public:
  explicit MaskTrackDataLayer(const LayerParameter& param)
    : ImageDimPrefetchingDataLayer<Dtype>(param) {}
  virtual ~MaskTrackDataLayer();
  virtual void DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "MaskTrackData"; }
  virtual inline int ExactNumBottomBlobs() const { return 0; }
  virtual inline int ExactNumTopBlobs() const { return 3; }
  virtual inline bool AutoTopBlobs() const { return true; }

 protected:
  virtual void ShuffleImages();
  virtual void load_batch(Batch<Dtype>* batch);

  Blob<Dtype> transformed_label_;
  shared_ptr<Caffe::RNG> prefetch_rng_;
  vector<std::tuple<std::string, std::string, std::string> > lines_;
  int lines_id_;
};

}  // namespace caffe

#endif  // CAFFE_IMAGE_SEG_DATA_LAYER_HPP_
