#!/usr/bin/python
import requests
import subprocess as sp
import xml.etree.ElementTree
import argparse
import time
import sys


def send_notification(dry_run=False):
    print('sending email notification')
    if dry_run:
        return False
    return requests.post(
        "https://api.mailgun.net/v3/sandboxeac54f030bdc4068b00b00370b3bbf88.mailgun.org/messages",
        auth=("api", "key-f2423d136453f8103db7ae2c6673fb96"),
        data={"from": "Mailgun Sandbox <postmaster@sandboxeac54f030bdc4068b00b00370b3bbf88.mailgun.org>",
              "to": "loskutak <jonas.serych@gmail.com>",
              "subject": "Halmos notification",
              "text": "Na Halmosu je misto!"})


def free_gpu(min_mem):
    gpu_info_cmd = ['nvidia-smi', '--query-gpu=memory.total,memory.used,memory.free', '--format=csv,noheader']
    proc = sp.Popen(gpu_info_cmd, stdout=sp.PIPE, stderr=sp.PIPE)
    gpu_stdout, gpu_stderr = proc.communicate()
    gpu_infos = gpu_stdout.strip().split('\n')
    gpu_infos = map(lambda x: x.split(', '), gpu_infos)
    gpu_infos = [{'mem_total': x[0], 'mem_used': x[1], 'mem_free': x[2]}
                 for x in gpu_infos]
    hope = False
    for id, gpu_info in enumerate(gpu_infos):
        mem_free = gpu_info['mem_free']
        mem_free_MiB = int(mem_free.split()[0])
        if mem_free_MiB > min_mem:
            return id

        mem_total = gpu_info['mem_total']
        mem_total_MiB = int(mem_total.split()[0])
        if mem_total_MiB > min_mem:
            hope = True

    if not hope:
        return -1
    else:
        return None


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("min_mem", type=int, help="minimum free GPU memory in MiB",
                        default=10000, nargs="?")
    args = parser.parse_args()
    print('Waiting for a suitable GPU')

    first_iter = True
    while True:
        gpu_id = free_gpu(args.min_mem)
        if gpu_id is not None:
            if gpu_id < 0:
                print('no sutable GPU on this computer!')
                sys.exit(1)  # there is no chance this amount of memory will be available
            print('found empty GPU: {}'.format(gpu_id))
            if first_iter:
                break  # no need to send email when GPU found immediately
            send_notification(dry_run=False)
            break
        time.sleep(15)
        first_iter = False
