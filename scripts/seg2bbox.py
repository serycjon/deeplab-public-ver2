#!/usr/bin/python
import sys
import os
import numpy as np
import cv2


def makedirp(path):
    if not os.path.exists(path):
        os.makedirs(path)


def convert(mask_path):
    mask = cv2.imread(mask_path, 0)
    points = cv2.findNonZero(mask)

    x, y, w, h = cv2.boundingRect(points)
    rows, cols = mask.shape
    bbox = np.zeros((rows, cols), np.uint8)
    bbox[y:y+h, x:x+w] = 255
    return bbox


def convert_dir(in_dir, out_dir):
    for root, dirs, files in os.walk(in_dir):
        for f in files:
            # try:
            print("converting {}".format(f))
            bbox = convert(os.path.join(root, f))
            cv2.imwrite(os.path.join(out_dir, f), bbox)
            # except:
            #     print("failed to convert {}".format(f))


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("usage: {} in_dir".format(sys.argv[0]))
        sys.exit(1)
    masks_dir = sys.argv[1]
    if not os.path.isdir(masks_dir):
        print("invalid in_dir!")
        sys.exit(1)

    bbox_dir = os.path.join(os.path.dirname(masks_dir), "bboxes")
    makedirp(bbox_dir)
    convert_dir(masks_dir, bbox_dir)
    print("Conversion finished.")
    print("Bounding box segmentations in {}".format(bbox_dir))
