#!/usr/bin/python
import os
import sys
import utils
import numpy as np
import cv2
import argparse
import interactive_input
import time
import statistics_accumulator


def fill_in_variables(string, var_map):
    for key, val in var_map.iteritems():
        string = string.replace("${" + key + "}", str(val))
    return string


def read_fill_write(template_path, out_path, var_map):
    print('creating {} from template'.format(os.path.basename(out_path)))
    with open(template_path, 'r') as fin:
        template = fin.read()
        filled = fill_in_variables(template, var_map)
        with open(out_path, 'w') as fout:
            fout.write(filled)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpu', type=int, help='GPU id (default auto-pick)')

    subparsers = parser.add_subparsers(title='subcommands',
                                       # description='valid subcommands',
                                       help='available commands',
                                       dest='subparser_name')

    train_parser = subparsers.add_parser('train')
    train_parser.add_argument('--batch', type=int, default=10, help='batch size')
    train_parser.add_argument('experiment', type=str, help='experiment name')
    train_parser.add_argument('--init-model', type=str,
                              help='path to initial caffemodel')
    train_parser.add_argument('--init-snapshot', type=int,
                              help='init snapshot iteration')

    test_parser = subparsers.add_parser('test')

    deploy_parser = subparsers.add_parser('deploy')

    args = vars(parser.parse_args())
    # print(args)
    # sys.exit(1)
    return args


def get_solver_params_dict(solver_config):
    solver_params = {}
    solver_params['test_iter'] = solver_config.test_iter[0]
    solver_params['test_interval'] = solver_config.test_interval
    solver_params['max_iter'] = solver_config.max_iter
    solver_params['snapshot'] = solver_config.snapshot
    return solver_params


if __name__ == '__main__':
    args = parse_arguments()
    settings = utils.get_local_settings()

    root_dir = settings['root_dir']
    caffe_dir = os.path.join(root_dir, 'deeplab', 'deeplab', 'python')

    tmp_dir = '/tmp/coin_tracking/'
    # reduce caffe outputs
    # os.environ['GLOG_minloglevel'] = '2'
    sys.path.insert(0, caffe_dir)

    if args['gpu'] is None:
        gpu_id = utils.pick_best_gpu(10000)
    else:
        gpu_id = args['gpu']

    print('going to use GPU {}'.format(gpu_id))

    if args['subparser_name'] == 'train':
        do_train = 1
        batch_sz = args['batch']
    elif args['subparser_name'] == 'deploy':
        do_deploy = 0

    experiment = args['experiment']
    if experiment == 'first':
        data_root = os.path.join(root_dir, 'data/MSRA10K')
    elif experiment == 'voc12':
        data_root = os.path.join(root_dir, 'data/VOCdevkit/VOC2012')
    else:
        print('unknown experiment name!')
        sys.exit(1)

    experiment_dir = os.path.join(root_dir, 'deeplab', 'deeplab',
                                  'networks', 'masktrack', experiment)

    net_id = 'masktrack'

    config_dir = os.path.join(experiment_dir, 'config', net_id)
    model_dir = os.path.join(experiment_dir, 'model', net_id)
    utils.mkdir(model_dir)
    log_dir = os.path.join(experiment_dir, 'log', net_id)
    utils.mkdir(log_dir)
    # bug: does not work
    # os.environ['GLOG_log_dir'] = log_dir

    real_stdout = sys.stdout
    # sys.stdout = open(os.path.join(log_dir, train
    import caffe

    if gpu_id >= 0:
        caffe.set_mode_gpu()
        caffe.set_device(gpu_id)
    else:
        caffe.set_mode_cpu()

    var_map = {}
    var_map['EXP'] = experiment
    var_map['NET_ID'] = net_id
    var_map['EXP_DIR'] = experiment_dir
    var_map['NUM_LABELS'] = 2 + 1
    var_map['BATCH_SIZE'] = batch_sz

    if do_train:
        data_root = utils.tmpize_dataset(data_root)
        var_map['DATA_ROOT'] = data_root
        list_dir = os.path.join(experiment_dir, 'file_lists')
        var_map['LIST_DIR'] = list_dir

        train_set = 'train'
        val_set = 'val'

        var_map['TRAIN_SET'] = train_set
        var_map['VAL_SET'] = val_set

        model = os.path.join(
            experiment_dir, 'model', net_id, 'init.caffemodel')
        train_prototxt_template = os.path.join(
            config_dir, 'train_template.prototxt')
        solver_prototxt_template = os.path.join(
            config_dir, 'solver_template.prototxt')

        train_prototxt = os.path.join(config_dir,
                                      'train_{}.prototxt'.format(train_set))
        solver_prototxt = os.path.join(config_dir,
                                       'solver_{}.prototxt'.format(train_set))

        read_fill_write(train_prototxt_template, train_prototxt, var_map)
        read_fill_write(solver_prototxt_template, solver_prototxt, var_map)

        from caffe.proto import caffe_pb2
        from google.protobuf import text_format

        solver_config = caffe_pb2.SolverParameter()
        with open(solver_prototxt, 'r') as f:
            text_format.Merge(str(f.read()), solver_config)

        solver_params = get_solver_params_dict(solver_config)

        solver = caffe.SGDSolver(solver_prototxt)
        statistics = statistics_accumulator.StatisticsAccumulator()

        # resume training
        init_snapshot = args['init_snapshot']
        if init_snapshot is not None:
            state_name = 'train_iter_{}'.format(init_snapshot)
            state_fname = '{}.solverstate'.format(state_name)
            state_path = os.path.join(model_dir, state_fname)

            solver.restore(state_path)

            stats_fname = '{}.yaml'.format(state_name)
            stats_path = os.path.join(model_dir, stats_fname)

            statistics.load_from(stats_path)

        # or start new one
        init_model = args['init_model']
        if init_snapshot is None:
            if init_model is None:
                model_path = os.path.join(model_dir, 'init.caffemodel')
            else:
                model_path = os.path.join(model_dir, init_model)
            solver.net.copy_from(model_path)

        # make the test network use the trained weights
        solver.test_nets[0].share_with(solver.net)

        ii = interactive_input.InteractiveInput()
        ii.start()


        step_size = 5
        while True:
            stats_path = os.path.join(model_dir,
                                        'train_iter_{}.yaml'.format(solver.iter))
            c = ii.get_char()
            if c is not None:
                if c == 'esc' or c == 'q':
                    print('do you really  want to quit? (y/N)')
                    if ii.get_char(blocking=True) == 'y':
                        print('do you want to save a snapshot? (Y/n)')
                        c = ii.get_char(blocking=True)

                        if c != 'n':
                            solver.snapshot()
                            statistics.save_to(stats_path)
                        else:
                            print('ok... your choice...')

                        ii.stop()
                        break
                    else:
                        print('unknown key: {}'.format(c))
                elif c == 'w':
                    print('writing snapshot')
                    solver.snapshot()
                    statistics.save_to(stats_path)

            solver.step(step_size)

            if solver.iter % solver_params['test_interval'] == 0:
                for test_iter in range(solver_params['test_iter']):
                    solver.test_nets[0].forward()

            stats_path = os.path.join(model_dir,
                                      'train_iter_{}.yaml'.format(solver.iter))
            # aggregate the outputs
            statistics.accumulate(solver)
            if (solver.iter % solver_params['snapshot']) == 0:
                solver.snapshot()
                statistics.save_to(stats_path)

            if solver.iter == solver_params['max_iter']:
                solver.snapshot()
                statistics.save_to(stats_path)
                ii.stop()
                break

            time.sleep(0.002)

    elif do_deploy:
        print('going to run the network, yaay!')
        train_set = 'train'
        deploy_prototxt_template = os.path.join(config_dir,
                                                'deploy_template.prototxt')
        deploy_prototxt = os.path.join(config_dir,
                                      'deploy.prototxt')
        read_fill_write(deploy_prototxt_template, deploy_prototxt, var_map)

        model_file = os.path.join(model_dir, 'train_iter_2000.caffemodel')

        net = caffe.Net(deploy_prototxt, model_file, caffe.TEST)

        val_mini = [11297, 153234, 18699, 11790, 161231, 203068,
                    101434, 30158, 109214, 122783, 116634, 30731,
                    60094, 31445, 133167, 5116, 45237, 181066, 195513,
                    165128, 88379, 51856, 150725, 5308, 180781]

        for test_img_name in val_mini:
            # test_img_name = '18358' # train set
            # test_img_name = '18699' # val set

            test_img = os.path.join(data_root, 'images',
                                    '{}.jpg'.format(test_img_name))
            test_bbox = os.path.join(data_root, 'bboxes',
                                    '{}.png'.format(test_img_name))

            img = cv2.imread(test_img)
            cv2.imwrite(os.path.join(tmp_dir,
                                     "{}_in_img.png".format(test_img_name)), img)
            bbox = cv2.imread(test_bbox, 0)
            bbox[bbox == 255] = 1

            h, w = bbox.shape
            net.blobs['data'].reshape(1, 4, h, w)

            net.blobs['data'].data[0, :3, :, :] = np.rollaxis(img[...], 2, 0)
            net.blobs['data'].data[0, 3, :, :] = bbox[...]

            net.forward()

            out = net.blobs['output'].data[0, :, :, :]
            out_class = np.argmax(out, 0)
            out_class[out_class == 2] = 0
            out_class[out_class == 1] = 255
            # print('output class shape: {}'.format(out_class.shape))
            out_class_big = cv2.resize(out_class, (w, h), interpolation=cv2.INTER_NEAREST)

            cv2.imwrite(os.path.join(tmp_dir, "{}_out.png".format(test_img_name)),
                                     out_class_big)
            cv2.imwrite(os.path.join(tmp_dir,
                                     "{}_in_bbox.png".format(test_img_name)),
                                     bbox*255)
