from __future__ import print_function
import sys
from subprocess import Popen, PIPE
import os
import shutil
import yaml


def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

        
def tmpize_dataset(root_dir):
    print('copying {} to /tmp/serycjon/... this may take some time'.format(root_dir))
    dataset_name = os.path.basename(os.path.normpath(root_dir))
    tmp_path = '/tmp/serycjon/'
    rsync_cmd = ['/usr/bin/rsync', '-aL', root_dir, tmp_path]
    print('cmd: {}'.format(rsync_cmd))

    proc = Popen(rsync_cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()

    tmp_path = os.path.join(tmp_path, dataset_name)

    # copytree(root_dir, tmp_path)
    print('dataset tmpized ({})'.format(tmp_path))
    return tmp_path


def err_print(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_local_settings():
    with open('local_settings.yaml', 'r') as fin:
        return yaml.load(fin.read())


def pick_best_gpu(min_mem=0):
    proc = Popen(["nvidia-smi",
                  "--query-gpu=index,memory.free,utilization.gpu",
                  "--format=csv,noheader,nounits"],
                 stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    gpus = [line.split(', ')
            for line in stdout.splitlines()]
    best_gpu = -1
    best_usage = 100
    for id, memfree, usage in gpus:
        id = int(id)
        memfree = int(memfree)
        try:
            usage = int(usage)
        except:
            err_print('WARNING: nvidia-smi usage comparison not supported')
            return -1

        if memfree < min_mem:
            continue
        if usage < best_usage:
            best_usage = usage
            best_gpu = id

    if best_gpu == -1:
        return -1

    if len(gpus) == 4:
        # Halmos
        err_print('Using Halmos GPU id translation table.')
        translate = {0: 3, 1: 1, 2: 2, 3: 0}
    else:
        translate = {x: x for x in range(len(gpus))}

    return translate[best_gpu]


if __name__ == '__main__':
    print(pick_best_gpu())
