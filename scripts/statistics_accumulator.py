import numpy as np
import yaml


class StatisticsAccumulator:
    def __init__(self):
        self.iteration = []

        self.loss = []
        self.accuracy = []

        self.val_loss = []
        self.val_accuracy = []

    def accumulate(self, solver):
        iteration = solver.iter

        loss = solver.net.blobs['loss'].data
        accuracy = np.squeeze(solver.net.blobs['accuracy'].data)

        test_net = solver.test_nets[0]
        val_loss = test_net.blobs['loss'].data
        val_accuracy = np.squeeze(test_net.blobs['accuracy'].data)

        self.iteration.append(iteration)
        self.loss.append(np.asscalar(loss))
        self.accuracy.append(accuracy.tolist())
        self.val_loss.append(np.asscalar(val_loss))
        self.val_accuracy.append(val_accuracy.tolist())

    def load_from(self, filename):
        with open(filename, 'r') as fin:
            node = yaml.load(fin.read())
            self.iteration = node['iteration']

            self.loss = node['loss']
            self.accuracy = node['accuracy']

            self.val_loss = node['val_loss']
            self.val_accuracy = node['val_accuracy']

            print('statistics loaded from {}'.format(filename))

    def save_to(self, filename):
        node = {}
        node['iteration'] = self.iteration

        node['loss'] = self.loss
        node['accuracy'] = self.accuracy

        node['val_loss'] = self.val_loss
        node['val_accuracy'] = self.val_accuracy

        output = yaml.dump(node)

        with open(filename, 'w') as fout:
            fout.write(output)
        print('statistics saved to {}'.format(filename))


if __name__ == '__main__':
    stats = StatisticsAccumulator()
    stats.save_to('/tmp/blab.yaml')
    stats.load_from('/tmp/blab.yaml')


