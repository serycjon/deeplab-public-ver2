#!/usr/bin/python
import os
import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt
import utils


def get_all_segmentations(seg_path):
    segmentations = []
    img = cv2.imread(seg_path)
    # cv2.imshow("blab", img)
    # cv2.waitKey(0)
    h, w, ch = img.shape
    dtype = img.dtype.descr * ch
    struct = img.view(dtype)
    uniq, inv = np.unique(struct, return_inverse=True)
    inv = inv.reshape(h, w)
    # cv2.imshow("inv", np.array(inv, dtype=np.uint8)*(255/2))
    # cv2.waitKey(0)
    uniq = uniq.view(img.dtype).reshape(-1, ch)
    n_objects = uniq.shape[0]

    background = np.array([0, 0, 0])
    ignore = np.array([192, 224, 224])
    background_label = -1
    ignore_label = -1

    for i in range(n_objects):
        color = uniq[i]
        if np.all(color == background):
            background_label = i
        if np.all(color == ignore):
            ignore_label = i

    for i in range(n_objects):
        if i == background_label or i == ignore_label:
            continue

        single_seg = np.copy(inv)
        mask = single_seg != i
        # TODO: also export ignore label, when masktrack data layer is ready
        # mask = np.logical_and((single_seg != i),
        #                       (single_seg != ignore_label))
        single_seg[mask] = background_label
        single_seg[np.logical_not(mask)] = 255
        single_seg = single_seg.astype(np.uint8)

        segmentations.append(single_seg)

    return segmentations


if __name__ == '__main__':
    root_path = utils.get_local_settings()['root_dir']
    data_path = os.path.join(root_path, 'data')
    voc12_path = os.path.join(data_path, 'VOCdevkit', 'VOC2012')
    images_dir = os.path.join(voc12_path, 'JPEGImages')
    mask_dir = os.path.join(voc12_path, 'SegmentationObject')

    out_mask_dir_name = 'SingleSegmentation'
    out_mask_dir = os.path.join(voc12_path, out_mask_dir_name)

    out_list_path = os.path.join(voc12_path, 'single_seg_list.txt')
    failed_list_path = os.path.join(voc12_path, 'failed.txt')

    utils.mkdir(out_mask_dir)

    if not (os.path.isdir(images_dir) and os.path.isdir(mask_dir)):
        print('incorrect paths:')
        print(images_dir)
        print(mask_dir)
        sys.exit(1)

    filelist = []
    failed = []

    for root, dirs, files in os.walk(mask_dir):
        for file_id, file in zip(range(len(files)), files):
            img_name, ext = os.path.splitext(file)
            seg_path = os.path.join(mask_dir, '{}.png'.format(img_name))

            progress = (100.0 * file_id) / len(files)
            print('{:0>4.1f}% - processing {}'.format(progress, img_name))
            try:
                segmentations = get_all_segmentations(seg_path)

                for i in range(len(segmentations)):
                    out_name = '{}_obj{:0>2}.png'.format(img_name, i)
                    im_rel_path = os.path.join('JPEGImages',
                                               '{}.jpg'.format(img_name))
                    # im_path = os.path.join(voc12_path, im_rel_path)
                    out_mask_rel_path = os.path.join(out_mask_dir_name,
                                                     out_name)
                    out_mask_path = os.path.join(voc12_path,
                                                 out_mask_rel_path)
                    cv2.imwrite(out_mask_path, segmentations[i])

                    filelist.append((im_rel_path, out_mask_rel_path))
            except:
                failed.append(im_rel_path)

    with open(out_list_path, 'w') as fout:
        lines = map(lambda x: ' '.join(x), filelist)
        out = '\n'.join(lines)
        fout.write(out)

    with open(failed_list_path, 'w') as fout:
        out = '\n'.join(failed)
        fout.write(out)
