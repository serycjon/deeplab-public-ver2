import matplotlib.pyplot as plt
import os
import sys
import masktrack
import cv2
import numpy as np
from selector import Selector
import argparse
import utils


def visualize_weights(net, layer):
    params = net.params[layer][0].data
    # plt.figure()
    # plt.imshow(params[0, 0, :, :], cmap='gray')
    # plt.show()
    print(params.shape)
    for i in range(params.shape[0]):
        print(np.std(params[i, :3, :, :]))
        print(np.std(params[i, 3, :, :]))
        print()


class GUI:
    def __init__(self, img_path, net, options={}):
        self.net = net
        self.fig = plt.figure()
        self.cidkey = self.fig.canvas.mpl_connect(
            'key_press_event', self.on_press)
        self.img_path = img_path
        self.img = []
        self.selector = Selector(self.fig, self.on_select)
        self.set_img(img_path)
        self.show()

    def set_img(self, path):
        self.img = cv2.imread(path)
        hmax = 400
        if self.img.shape[0] > hmax:
            ratio = float(hmax) / self.img.shape[0]
            new_w = int(self.img.shape[1] * ratio)
            print('image too big....')
            print('going to resize: ratio {}. new size {}x{}'.
                  format(ratio, new_w, hmax))
            self.img = cv2.resize(self.img, (new_w, hmax))

    def show(self):
        plt.imshow(cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB))
        while True:
            try:
                plt.pause(0.05)
            except:
                break

    def on_press(self, event):
        if event.key == 'q':
            plt.close('all')
            sys.exit(1)
        elif event.key == 'r':
            plt.imshow(cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB))
        else:
            print('no keybinding for: {}'.format(event.key))

    def draw_segmentation(self, seg_mask, img=None):
        if img is None:
            img = self.img

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        mask = cv2.cvtColor(seg_mask, cv2.COLOR_GRAY2RGB)
        mask[:, :, 0] = 0
        mask[:, :, 2] = 0
        mask[mask > 0] = 255

        blend = cv2.addWeighted(img, 1, mask, 0.5, 0.0)

        plt.figure(self.fig.number)
        plt.imshow(blend)
        plt.draw()

    def on_select(self, corner_1, corner_2):
        print('selected from {} to {}'.format(corner_1, corner_2))
        corner_1 = (int(corner_1[0]), int(corner_1[1]))
        corner_2 = (int(corner_2[0]), int(corner_2[1]))
        corners = (corner_1, corner_2)
        # bbox_mask = self.net.bbox2mask(self.img, corners)
        # plt.figure(2)
        # plt.imshow(bbox_mask, cmap='gray')
        if ('cut' in options) and options['cut']:
            min_r = min(corner_1[0], corner_2[0])
            max_r = max(corner_1[0], corner_2[0])

            min_c = min(corner_1[1], corner_2[1])
            max_c = max(corner_1[1], corner_2[1])

            roi = self.img[min_c:max_c, min_r:max_r]
            out = self.net.forward_bbox(roi, ((0, 0),
                                              (roi.shape[0], roi.shape[1])))
            self.draw_segmentation(out, roi)
        else:
            out = self.net.forward_bbox(self.img, corners)
            # plt.figure(3)
            # plt.imshow(out, cmap='gray')
            # plt.draw()

            self.draw_segmentation(out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    root_dir = utils.get_local_settings()['root_dir']
    default_im_path = os.path.join(root_dir, 'data', 'MSRA10K',
                                   'images', '18699.jpg')
    parser.add_argument('im_path', nargs='?', type=str,
                        default=default_im_path,
                        help='image path')
    parser.add_argument('-e', '--experiment', required=True, type=str,
                        help='experiment name')
    parser.add_argument('-s', '--snapshot', type=int,
                        default=20000,
                        help='caffemodel snapshot number')
    parser.add_argument('--cut', action='store_true',
                        help='use bbox to cut a ROI')

    args = vars(parser.parse_args())
    options = {}
    if args['cut']:
        options['cut'] = True

    base_dir = os.path.join(root_dir, 'deeplab', 'deeplab')
    caffe_dir = os.path.join(base_dir, 'python')
    experiment = args['experiment']
    net_id = 'masktrack'

    experiment_dir = os.path.join(base_dir, 'networks',
                                  'masktrack', experiment)
    config_dir = os.path.join(experiment_dir, 'config', net_id)
    model_dir = os.path.join(experiment_dir, 'model', net_id)

    prototxt = os.path.join(config_dir, 'deploy.prototxt')
    snapshot = args['snapshot']
    model = os.path.join(model_dir,
                         'train_iter_{}.caffemodel'.format(snapshot))

    net = masktrack.Masktrack(prototxt, model, caffe_dir)
    # visualize_weights(net.net, 'conv1_1_surg')
    # sys.exit(1)

    im_path = args['im_path']
    gui = GUI(im_path, net, options)
