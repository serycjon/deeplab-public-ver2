#!/usr/bin/env python2
import os
import sys
import utils
import numpy as np
import cv2
# import matplotlib.pyplot as plt


class Masktrack:
    def __init__(self, deploy_prototxt, model_file, caffe_path,
                 gpu_id=None, debug=False):
        "haha, TODO documentation..."
        sys.path.insert(0, caffe_path)
        if not debug:
            os.environ['GLOG_minloglevel'] = '2'
        import caffe

        if gpu_id is None:
            gpu_id = utils.pick_best_gpu(7000)
        self.gpu_id = gpu_id

        if gpu_id >= 0:
            caffe.set_mode_gpu()
            caffe.set_device(gpu_id)
            print('GPU mode on GPU-{}'.format(gpu_id))
        else:
            caffe.set_mode_cpu()
            print('CPU mode')

        self.net = caffe.Net(deploy_prototxt, model_file, caffe.TEST)
        print('masktrack initialized')

    def bbox2mask(self, img, corners):
        mask = np.zeros((img.shape[0], img.shape[1]),
                        np.uint8)

        mask = cv2.rectangle(mask, corners[0], corners[1], 255, -1)
        return mask

    def forward_bbox(self, img, corners, binary=False):
        mask = self.bbox2mask(img, corners)
        out_mask = self.forward(img, mask, binary=True)
        if not binary:
            out_mask[out_mask == 1] = 255

        return out_mask

    def forward(self, img, mask, binary=False):
        h, w = mask.shape

        if not binary:
            mask[mask == 255] = 1

        net = self.net
        net.blobs['data'].reshape(1, 4, h, w)

        net.blobs['data'].data[0, :3, :, :] = np.rollaxis(img[...], 2, 0)
        net.blobs['data'].data[0, 3, :, :] = mask[...]

        net.forward()

        out_class = np.argmax(net.blobs['output'].data[0, :, :, :], 0)
        out_class = np.array(out_class, dtype='uint8')
        out_class[out_class == 2] = 0  # outside of img class
        if not binary:
            out_class[out_class == 1] = 255

        out_class = cv2.resize(out_class, (w, h),
                               interpolation=cv2.INTER_NEAREST)
        return out_class


if __name__ == '__main__':
    root_dir = utils.get_local_settings()['root_dir']
    base_dir = os.path.join(root_dir, 'deeplab', 'deeplab')
    caffe_dir = os.path.join(base_dir, 'python')
    experiment = 'first'
    net_id = 'masktrack'

    experiment_dir = os.path.join(base_dir, 'networks',
                                  'masktrack', experiment)
    config_dir = os.path.join(experiment_dir, 'config', net_id)
    model_dir = os.path.join(experiment_dir, 'model', net_id)

    prototxt = os.path.join(config_dir, 'deploy.prototxt')
    model = os.path.join(model_dir, 'train_iter_2000.caffemodel')

    net = Masktrack(prototxt, model, caffe_dir)

    img_id = 18699
    data_dir = os.path.join(root_dir, 'data')
    dataset_dir = os.path.join(data_dir, 'MSRA10K')
    test_img = os.path.join(dataset_dir, 'images', '{}.jpg' \
                            .format(img_id))
    test_bbox = os.path.join(dataset_dir, 'bboxes', '{}.png' \
                             .format(img_id))

    img = cv2.imread(test_img)
    bbox = cv2.imread(test_bbox, 0)

    # corners = ((56, 65),
    #            (270, 321))
    # segmentation = net.forward_bbox(img, corners)
    segmentation = net.forward(img, bbox)
    print('yaay')

    import matplotlib.pyplot as plt
    plt.figure()
    plt.imshow(segmentation, cmap='gray')
    plt.figure()
    plt.imshow(img)
    plt.show()
