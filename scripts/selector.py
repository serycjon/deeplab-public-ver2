import matplotlib.pyplot as plt
import matplotlib.patches


class Selector:
    def __init__(self, fig, callback):
        self.callback = callback
        self.fig = fig
        self.press = False
        self.rectangle = matplotlib.patches.Rectangle((0, 0), 0, 0, fill=None)
        self.rectangle.set_visible(False)
        self.axes = plt.gca()  # FIXME: get the right axes somewhere else
        self.axes.add_patch(self.rectangle)
        self.connect()

    def __del__(self):
        self.disconnect()

    def connect(self):
        self.cidpress = self.fig.canvas.mpl_connect(
            'button_press_event', self.on_press)
        self.cidrelease = self.fig.canvas.mpl_connect(
            'button_release_event', self.on_release)
        self.cidmotion = self.fig.canvas.mpl_connect(
            'motion_notify_event', self.on_motion)

    def disconnect(self):
        self.fig.canvas.mpl_disconnect(self.cidpress)
        self.fig.canvas.mpl_disconnect(self.cidrelease)
        self.fig.canvas.mpl_disconnect(self.cidmotion)

    def on_press(self, event):
        if plt.get_current_fig_manager().toolbar.mode != '':
            return
        self.corner_1 = (event.xdata, event.ydata)
        self.press = True
        self.rectangle.set_bounds(event.xdata, event.ydata, 0, 0)
        self.rectangle.set_visible(True)

    def on_motion(self, event):
        if plt.get_current_fig_manager().toolbar.mode != '':
            return
        if not self.press:
            return
        self.corner_2 = (event.xdata, event.ydata)
        bounds = self.corners_to_plt_rect((self.corner_1, self.corner_2))
        self.rectangle.set_bounds(*bounds)

    def on_release(self, event):
        if plt.get_current_fig_manager().toolbar.mode != '':
            return
        self.corner_2 = (event.xdata, event.ydata)
        self.press = False
        self.callback(self.corner_1, self.corner_2)

    def corners_to_plt_rect(self, corners):
        """convert from pair of oposite rectangle corners to the matplotlib
           rectangle bounds, i.e. left, bottom, width, height. """
        l = min(corners[0][0], corners[1][0])
        r = max(corners[0][0], corners[1][0])

        b = min(corners[0][1], corners[1][1])
        t = max(corners[0][1], corners[1][1])

        w = r - l
        h = t - b

        return (l, b, w, h)
