import sys
import select
import tty
import termios
import time
import Queue
import threading


class InteractiveInput:
    """Read characters from stdin. Nonblocking, no newline needed

    main source: http://stackoverflow.com/a/2409034/1705970
    (python nonblocking console input)

    the nonblocking interface: http://stackoverflow.com/a/18990710/1705970
    (execute python function in main thread from call in dummy thread)
    """
    def __init__(self):
        self.running = False
        self.queue = Queue.Queue()

    def is_data(self):
        return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

    def add_to_queue(self, char):
        self.queue.put(char)

    def get_char(self, blocking=False):
        if blocking:
            return self.queue.get()
        else:
            try:
                char = self.queue.get(False)  # non-blocking
            except Queue.Empty:
                char = None
            return char

    def stop(self):
        self.running = False

    def start(self):
        threading.Thread(target=self.run).start()

    def run(self):
        if self.running:
            return
        else:
            self.running = True

        old_settings = termios.tcgetattr(sys.stdin)
        try:
            tty.setcbreak(sys.stdin.fileno())

            while self.running:
                if self.is_data():
                    c = sys.stdin.read(1)

                    if c == '\x1b':  # x1b (=27) is ESC
                        c = 'esc'

                    self.add_to_queue(c)
                time.sleep(0.02)

        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_settings)
            self.running = False


if __name__ == '__main__':
    ii = InteractiveInput()
    ii.start()

    while True:
        c = ii.get_char()
        if c is not None:
            if c == 'esc':
                print('do you really want to exit? (y/N)')
                if ii.get_char(blocking=True) == 'y':
                    ii.stop()
                    break
            else:
                print('v hlavnim threadu ctu {}'.format(c))
        time.sleep(0.02)
