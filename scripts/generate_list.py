#!/usr/bin/python
import sys
import os
import utils
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', type=str)
    parser.add_argument('out_file', type=str)
    args = parser.parse_args()

    dataset = args.dataset
    out_path = args.out_file
    # out_path = '/home/jonas/dev/thesis/deeplab/masktrack/file_lists/train.txt'

    root_dir = utils.get_local_settings()['root_dir']
    data_dir = os.path.join(root_dir, 'data')

    out = ""
    if dataset == 'msra':
        image_dir = os.path.join(data_dir, 'MSRA10K', 'images')

        for root, dirs, files in os.walk(image_dir):
            for f in files:
                im_name, ext = os.path.splitext(f)
                img_path = os.path.join('/images', f)
                mask_path = os.path.join('/bboxes', '{}.png'.format(im_name))
                label_path = os.path.join('/masks', '{}.png'.format(im_name))
                out += "{} {} {}\n".format(img_path, mask_path, label_path)

        with open(out_path, 'w') as fout:
            fout.write(out)

        print("file list written to {}".format(out_path))
