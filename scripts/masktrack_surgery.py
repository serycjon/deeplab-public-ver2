#!/usr/bin/python
import os, sys

caffe_dir = '/home/jonas/dev/thesis/deeplab/deeplab'
# # reduce caffe outputs 
os.environ['GLOG_minloglevel'] = '2'
caffe_path = os.path.join(caffe_dir, 'python')
print(caffe_path)
sys.path.insert(0, caffe_path)
import caffe

gpu_id = -1
if gpu_id >= 0:
  caffe.set_mode_gpu()
  caffe.set_device(gpu_id)
else:
  caffe.set_mode_cpu()

masktrack_prototxt_file = '/home/jonas/dev/thesis/deeplab/masktrack/first/config/masktrack/train_train.prototxt'
prototxt_file = '/home/jonas/dev/thesis/deeplab/deeplab_exper/voc12/config/deeplab_largeFOV/train_train.prototxt'
model_file = '/home/jonas/dev/thesis/deeplab/deeplab_models/init.caffemodel'
out_file = '/home/jonas/dev/thesis/deeplab/masktrack/first/model/masktrack/init.caffemodel'

# instructions at
# http://stackoverflow.com/questions/39898291/net-surgery-how-to-reshape-a-convolution-layer-of-a-caffemodel-file-in-caffe
net = caffe.Net(prototxt_file, model_file, caffe.TRAIN)
empty_net = caffe.Net(masktrack_prototxt_file, model_file, caffe.TRAIN)

# list available blob/param names
# print("blobs {}\n\nparams {}".format(net.blobs.keys(), net.params.keys()))

empty_params = empty_net.params['conv1_1_surg'][0].data
train_params = net.params['conv1_1'][0].data

empty_params[:,:3,:,:] = train_params[...]

print('original conv1_1 shape: {}'.format(train_params.shape))
print('new conv1_1 shape: {}'.format(empty_params.shape))

print('')
print('original filter: ')
print(net.params['conv1_1'][0].data[0,:,:,:])

print('')
print('after surgery: ')
print(empty_net.params['conv1_1_surg'][0].data[0,:,:,:])

empty_net.save(out_file)
print('new model saved to {}'.format(out_file))
